#
# Copyright (C) 2016 The Android Open Source Project
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

LOCAL_PATH := $(my-dir)

bvb_common_cflags := \
    -D_FILE_OFFSET_BITS=64 \
    -D_POSIX_C_SOURCE=199309L \
    -Wa,--noexecstack \
    -Werror \
    -Wall \
    -Wextra \
    -Wformat=2 \
    -Wno-psabi \
    -Wno-unused-parameter \
    -ffunction-sections \
    -fstack-protector-strong \
    -fvisibility=hidden
bvb_common_cppflags := \
    -Wnon-virtual-dtor \
    -fno-strict-aliasing
bvb_common_ldflags := \
    -Wl,--gc-sections

include $(CLEAR_VARS)
LOCAL_SRC_FILES := bvbtool
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_IS_HOST_MODULE := true
LOCAL_MODULE := bvbtool
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libbvb_refimpl
LOCAL_MODULE_HOST_OS := linux
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_CLANG := true
LOCAL_CFLAGS := $(bvb_common_cflags) -fno-stack-protector -DBVB_ENABLE_DEBUG -DBVB_REFIMPL_COMPILATION
LOCAL_LDFLAGS := $(bvb_common_ldflags)
LOCAL_C_INCLUDES :=
LOCAL_SRC_FILES := \
    refimpl/bvb_property.c \
    refimpl/bvb_rsa.c \
    refimpl/bvb_sha256.c \
    refimpl/bvb_sha512.c \
    refimpl/bvb_util.c \
    refimpl/bvb_verify.c
include $(BUILD_HOST_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libbvb_refimpl_sysdeps
LOCAL_MODULE_HOST_OS := linux
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_CLANG := true
LOCAL_CFLAGS := $(bvb_common_cflags) -DBVB_ENABLE_DEBUG -DBVB_REFIMPL_COMPILATION
LOCAL_LDFLAGS := $(bvb_common_ldflags)
LOCAL_C_INCLUDES :=
LOCAL_SRC_FILES := \
    refimpl/bvb_sysdeps_stub.c
include $(BUILD_HOST_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libbvb_refimpl_unittest
LOCAL_MODULE_HOST_OS := linux
LOCAL_CPP_EXTENSION := .cc
LOCAL_CLANG := true
LOCAL_CFLAGS := $(bvb_common_cflags)
LOCAL_CPPFLAGS := $(bvb_common_cppflags)
LOCAL_LDFLAGS := $(bvb_common_ldflags)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/refimpl external/gtest/include
LOCAL_STATIC_LIBRARIES := \
    libbvb_refimpl \
    libbvb_refimpl_sysdeps \
    libgmock_host \
    libgtest_host
LOCAL_SHARED_LIBRARIES := \
    libchrome
LOCAL_SRC_FILES := \
    bvb_util_unittest.cc \
    bvb_verify_unittest.cc \
    bvbtool_unittest.cc
LOCAL_LDLIBS_linux := -lrt
include $(BUILD_HOST_NATIVE_TEST)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := bvb_refimpl_symbols_test
LOCAL_MODULE_TAGS := debug
LOCAL_ADDITIONAL_DEPENDENCIES := libbvb_refimpl
include $(BUILD_HOST_PREBUILT)
